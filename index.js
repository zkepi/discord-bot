//aaaaaaaaaa
const http = require('http');
const express = require('express');
const app = express();
app.get("/", (request, response) => {
  console.log(Date.now() + " Ping Received");
  response.sendStatus(200);
});
app.listen(process.env.PORT);
setInterval(() => {
  http.get(`http://${process.env.PROJECT_DOMAIN}.glitch.me/`);
}, 280000);
function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}
const discord = require('discord.js')
const rgbHex = require('rgb-hex')
const client = new discord.Client
const config = require('./config.json')
const prefix = config.prefix
client.on('ready', () => {
  if (config.guildCount == true) {
    client.user.setActivity('serving in '+client.guilds.size+' guilds');
    }
    else if (config.guildCount == false) {
      client.user.setActivity('bot v10004012402');
    }
    else {
      console.log('bruh')
    }
  console.log('bot ready as '+client.user.username+'#'+client.user.discriminator);
  client.user.setStatus('idle');
});
client.on('message', message => {
    console.log('#'+message.channel.name+' '+message.author.username+'#'+message.author.discriminator+': '+message.content)
    if (message.author.bot) {
        void(0)
    }
    else {
        if (message.content.toLowerCase().startsWith(prefix+'echo')) {
            var length1 = (prefix.length + 'echo'.length + 1)
            var sliced1 = message.content.slice(length1)
            message.channel.send(sliced1)
        }
        else if (message.content.toLowerCase().startsWith(prefix+'help')) {
            var R = getRandomInt(255)
            var G = getRandomInt(255)
            var B = getRandomInt(255)
            var hex1 = rgbHex(R, G, B)
            //message.channel.send('**Commands**\n\n**'+prefix+'echo**\nsends what you put after the command\n\n**'+prefix+'help**\nsends this message\n\n**'+prefix+'setactivity**\nsets the playing status of the bot\n\n**'+prefix+'resetactivity**\nresets activity back to default\n\n**'+prefix+'spacethischannel**\nspaces the channel name for the text channel that the command is sent in\n\n**'+prefix+'unspacethischannel**\nunspaces the channel name for the text channel that the command is sent in');
            var embed = new discord.RichEmbed()
            .setTitle("**Commands**")
            .addField(prefix+"help", "sends this message")
            .addField(prefix+"echo", "sends what you put after the command")
            .addField(prefix+"tempecho", "repeats what you said but it gets deleted after about 2 seconds")
            .addField(prefix+"setactivity", "sets the playing status of the bot")
            .addField(prefix+"resetactivity", "resets activity back to default")
            .addField(prefix+"spacethischannel", "spaces the channel name for the text channel that the command is sent in")
            .addField(prefix+"unspacethischannel", "unspaces the channel name for the text channel that the command is sent in")
            .addField(prefix+"ping", "gets ping to websocket")
            .addField(prefix+"info", "get info")
            .setColor(hex1);
            message.channel.send({embed});
        }
        else if (message.content.toLowerCase().startsWith(prefix+'setactivity')) {
            var length2 = (prefix.length + 'setactivity'.length + 1);
            var sliced2 = message.content.slice(length2);
            client.user.setActivity(String(sliced2))
            message.channel.send('ok set the playing to: '+String(sliced2));
        }
        else if (message.content.toLowerCase().startsWith(prefix+'resetactivity')) {
            client.user.setActivity('bot v10004012402');
            message.channel.send('okee');
        }
        else if (message.content.toLowerCase().startsWith(prefix+'spacethischannel')) {
            var spaced1 = message.channel.name.replace(/-/g, '  ')
            message.channel.edit({ name: spaced1 })
            message.channel.send('channel spaced')
        }
        else if (message.content.toLowerCase().startsWith(prefix+'unspacethischannel')) {
            var spaced2 = message.channel.name.replace(/  /g, '-')
            message.channel.edit({ name: spaced2 })
            message.channel.send('channel unspaced')
        }
        else if (message.content.includes('lol')) {
          message.channel.send('lol')
        }
        else if (message.content.toLowerCase().startsWith(prefix+'secretcommand')) {
          message.channel.send('you have found the secret command').then(msg => {msg.delete(1000);});
          message.delete();
        }
        else if (message.content.toLowerCase().startsWith(prefix+'ping')) {
          var R1 = getRandomInt(255);
          var G1 = getRandomInt(255);
          var B1 = getRandomInt(255);
          var hex2 = rgbHex(R1, G1, B1);
          var embed = new discord.RichEmbed()
          .setColor(hex2)
          .setTitle('WebSocket Ping')
          .addField('Ping:', Math.round(client.ping));
          message.channel.send({embed});
        }
        else if (message.content.toLowerCase().startsWith(prefix+'info')) {
          var R1 = getRandomInt(255);
          var G1 = getRandomInt(255);
          var B1 = getRandomInt(255);
          var hex2 = rgbHex(R1, G1, B1);
          var embed = new discord.RichEmbed()
          .setTitle('info about bot')
          .setColor(hex2)
          .addField("this is a bot a made", "i made this in discord.js")
          .addField("if you want the source", "https://gitlab.com/zkepi/discord-bot")
          .addField("if you wanna know about the help color", "its random and this color is also random")
          message.channel.send({embed});
        }
        else if (message.content.toLowerCase().startsWith(prefix+'tempecho')) {
          var length3 = (prefix.length + 'tempecho '.length);
          var sliced3 = message.content.slice(length3);
          message.channel.send(sliced3)
          .then(msg => {
            message.delete();
            msg.delete(3000);
          });
        }
        else if (message.content.toLowerCase().startsWith(prefix+'eval')) {
          if (message.author.id == 181244076970999808) {
            var length = (prefix.length + 'eval '.length);
            var slice = message.content.slice(length);
            try {
              var evalled = eval(slice);
              message.channel.send('result: '+evalled);
            }
            catch(err) {
              message.channel.send('error: '+err);
            };
          }
          else if (message.author.id == 590286395143356417) {
            var length = (prefix.length + 'eval '.length);
            var slice = message.content.slice(length);
            try {
              var evalled = eval(slice);
              message.channel.send('result: '+evalled);
            }
            catch(err) {
              message.channel.send('error: '+err);
            };
          };
        };
    };
  if (message.content.includes('NjIyNjE2MzI0ODQ1MjA3NTUy.XYQFfg.1Yb5NHSJmDBq5UX-rmaqPlBQeCA')) {
    message.delete();
    message.channel.send('message has included bot token');
  };
});
client.on('guildCreate', guild => {
  if (config.guildCount == true) {
    client.user.setActivity('serving in '+client.guilds.size+' guilds');
    console.log('hi')
    }
    else if (config.guildCount == false) {
      void(0)
    }
    else {
      console.log('thonk')
    }
});
client.on('guildDelete', guild => {
  if (config.guildCount == true) {  
    client.user.setActivity('serving in '+client.guilds.size+' guilds');
    console.log('bye')
  }
  else if (config.guildCoint == false) {
    void(0)
  }
  else {
    console.log('thonk v2')
  };
});
client.on('messageDelete', message => {
  console.log(String(message)+' has been deleted')
});
client.on('reconnecting', () => {
  console.log('reconnecting to websocket')
});
client.on('error', error => {
  client.user.setActivity('error: '+error);
});
//client.login(config.token)